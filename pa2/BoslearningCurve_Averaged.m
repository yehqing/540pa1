function [error_train, error_val,m] = ...
    learningCurve_Averaged(X, y, Xval, yval, lambda)
%LEARNINGCURVE_AVERAGED Generates the train and cross validation set errors needed 
%to plot a learning curve
%   [error_train, error_val] = ...
%       LEARNINGCURVE_AVERAGED(X, y, Xval, yval, lambda) returns the train and
%       validation set errors for a learning curve. In particular, 
%       it returns two vectors of the same length - error_train and 
%       error_val. Then, error_train(i) contains the training error for
%       i examples (and similarly for error_val(i)).
%
%   In this function, you will compute the train and test errors for
%   dataset sizes from 1 up to m. In practice, when working with larger
%   datasets, you might want to do this in larger intervals.
%   N random draws from the training set are used to calculate these errors
%   for a particular i.
%

% Number of training examples
N = 50;
m = 20;
%m = size(X, 1);
%m1 = size(Xval, 1);

% You need to return these values correctly
error_train = zeros(m, 1);
error_val   = zeros(m, 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return training errors in 
%               error_train and the cross validation errors in error_val. 
%               i.e., error_train(i) and 
%               error_val(i) should give you the errors
%               obtained after training on i examples.
%
% Note: You should evaluate the training error on i randomly chosen 
%       training examples 
%
%       For the validation set error, you should instead evaluate on
%       the _entire_ validation set (Xval and yval).
%
% Note: If you are using your cost function (linearRegCostFunction)
%       to compute the training and  validation error, you should 
%       call the function with the lambda argument set to 0. 
%       Do note that you will still need to use lambda when running
%       the training to obtain the theta parameters.
%


for index = 1:m
    error_train_tmp = 0;
    error_val_tmp = 0;
    
    for n = 1:N
        
        ran_x = [];
        ran_y = [];
        
        chosen_x_ids = RANDPERM(m);  % Pick index numbers from a list of m items
        
        for i = 1: index
           ran_x = [ ran_x ; X(chosen_x_ids(i) , :) ];
           ran_y = [ ran_y ; y(chosen_x_ids(i)) ];
        end
        
        ran_valx = [];
        ran_valy = [];
        
        chosen_x_ids = RANDPERM(m);  % Pick index numbers from a list of m items
        
        for i = 1: index
           ran_valx = [ ran_valx ; Xval(chosen_x_ids(i) , :)  ];
           ran_valy = [ ran_valy ; yval(chosen_x_ids(i)) ];
        end
        
        theta = trainLinearReg(ran_x, ran_y, lambda);  
        
        error_train_tmp = error_train_tmp + linearRegCostFunction(ran_x, ran_y, theta, 0);
        error_val_tmp = error_val_tmp + linearRegCostFunction( ran_valx, ran_valy, theta, 0);
    end
    
    error_train(index) = error_train_tmp / N ;
    error_val(index) = error_val_tmp / N ;
end


% -------------------------------------------------------------

% =========================================================================

end
