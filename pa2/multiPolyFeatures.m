function [ X_poly ] = multiPolyFeatures( X, d )
%UNTITLED2 Add features, including hybrid terms
%   Detailed explanation goes here


n=size(X,2);
m=size(X,1);
if d==1
    mat_size=n;
end

if d==2
    mat_size=n+factorial(2+n-1)/factorial(2)/factorial(n-1);
end
if d==3
    mat_size=n+factorial(2+n-1)/factorial(2)/factorial(n-1)+factorial(3+n-1)/factorial(3)/factorial(n-1);
end
X_poly=zeros(m,mat_size);
count=n;
for i=1:n
    X_poly(:,i)=X(:,i);
end
if d>=2
    for i=1:n
        for j=i:n
            count=count+1;
            for k=1:m
                X_poly(k,count)=X(k,i)*X(k,j);
            end
        end
    end
end
if d==3
    for i=1:n
        for j=(i+1):n
            for k=j:n
                count=count+1;
                for t=1:m
                    X_poly(t,count)=X(t,i)*X(t,j)*X(t,k);
                end
            end
        end
        for j=(i+1):n
            count=count+1;
            for k=1:m
                X_poly(k,count)=X(k,i)*X(k,i)*X(k,j);
            end
        end
        count=count+1;
        for j=1:m
            X_poly(j,count)=X(j,i)^3;
        end
    end                    
end    

end

