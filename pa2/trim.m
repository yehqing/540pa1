function [X_trim] = trim(X, list)

X_trim =[];
for i = 1: size(X,2)
    if(ismember(i,list))
    else
        X_trim = [X_trim X(:,i)];
    end
end

% ============================================================

end