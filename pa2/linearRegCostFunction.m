function [J, grad] = linearRegCostFunction(X, y, theta, lambda)
%LINEARREGCOSTFUNCTION Compute cost and gradient for regularized linear 
%regression with multiple variables
%   [J, grad] = LINEARREGCOSTFUNCTION(X, y, theta, lambda) computes the 
%   cost of using theta as the parameter for linear regression to fit the 
%   data points in X and y. Returns the cost in J and the gradient in grad

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost and gradient of regularized linear 
%               regression for a particular choice of theta.
%
%               You should set J to the cost and grad to the gradient.
%

sum1=0;
sum2=0;
n=size(theta);

for i=1:m
    sum1=sum1+(y(i)-transpose(theta)*transpose(X(i,:)))^2;
end
for i=1:n
    sum2=sum2+theta(i)^2;
end

J=sum1/2/m+lambda*sum2/2/m;

% =========================================================================
sum1=0;
for i=1:m
    sum1 = sum1 + (transpose(theta)*transpose(X(i,:)) - y(i) ) * X(i,1);
end
grad(1)=sum1/m;
for i=2:n
    sum1=0;
    for j=1:m
        sum1 = sum1 + ((transpose(theta)*transpose(X(j,:)) - y(j))*X(j,i) ) /m;
    end
    grad(i) = sum1 + lambda*theta(i) /m;
end
grad = grad(:);

end
