% Code for analyzing the boston housing data set using the techniques learned on
% the small data set. Please add comments to the code, so we can grade it
% with ease.




Trimlist = [4,1,5,2,6,7,3,8,10,9,12];
Jerror = zeros(length(Trimlist));  % Jerror(number of trimmed features) = error;   used for plotting

for id = 1:length(Trimlist)
    % Initialization
    clear data;

    % Loading Data
    data = load('housing.data.txt');

    % trim redundant data

    trimlist = Trimlist(1:id);
    data = trim(data, trimlist);

    % Divide the Data into Training, Validation and Test sets
    %{
    [trainData,valData,testData,trainInd,valInd,testInd]=dividerand(transpose(data));

    trainData=transpose(trainData);
    valData=transpose(valData);
    testData=transpose(testData);
    %}
    trainData=data(1:300,:);
    valData=data(301:400,:);
    testData=data(401:506,:);

    n = size(data,2);
    
    trainX=trainData(:,1:n-1);
    trainy=trainData(:, n);
    valX=valData(:,1:n-1);
    valy=valData(:, n);
    testX=testData(:,1:n-1);
    testy=testData(:, n);
    
    m=length(trainy);

    % Feature Mapping for Polynomial Regression
    d=1;
    trainX_poly = multiPolyFeatures( trainX, d );
    [trainX_poly, mu, sigma] = featureNormalize(trainX_poly);   % Normalize
    trainX_poly = [ones(m, 1), trainX_poly];  % Add Ones

    testX_poly = multiPolyFeatures(testX, d);
    testX_poly = bsxfun(@minus, testX_poly, mu);
    testX_poly = bsxfun(@rdivide, testX_poly, sigma);
    testX_poly = [ones(size(testX_poly, 1), 1), testX_poly];

    valX_poly = multiPolyFeatures(valX, d);
    valX_poly = bsxfun(@minus, valX_poly, mu);
    valX_poly = bsxfun(@rdivide, valX_poly, sigma);
    valX_poly = [ones(size(valX_poly, 1), 1), valX_poly];

    % Record Jerror of validation data, using lambda = 10, 
    theta = trainLinearReg(trainX_poly, trainy, 10);
    
    [J, grad] = linearRegCostFunction(valX_poly, valy, theta, 0);
    Jerror(id) = J;
    
end
    plot(1:length(Jerror), Jerror);

    title(sprintf('Trim Curve (d = %f)', d));
    xlabel('Number of trimmed featrues')
    ylabel('Error')
    axis([0 20 0 200])
    %legend('Train')