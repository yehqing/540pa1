function X_std = stdFeatures(X)
% standardize the columns of X so that they
% all have mean of 0 and unit variance

% You need to return the following variable correctly 
X_std = zeros(size(X));


% ====================== YOUR CODE HERE ======================

X_std = bsxfun(@minus, X, mean(X));
X_std = bsxfun(@rdivide, X_std, std(X_std) );

end
