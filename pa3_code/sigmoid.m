function g = sigmoid(z)
%SIGMOID Compute sigmoid functoon
%   J = SIGMOID(z) computes the sigmoid of z.

% You need to return the following variables correctly 
g = zeros(size(z));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the sigmoid of each value of z (z can be a matrix,
%               vector or scalar).

[m,n]=size(z);

if m==1 && n==1
    g=1/(1+exp(-z));
else if m==1 && n>1
        for i = 1:n
            g(m,i)=1/(1+exp(-z(m,i)));
        end
    else if m>1 && n==1
            for i = 1:m
                g(i,n)=1/(1+exp(-z(i,n)));
            end
        else
            for i = 1:m
                for j= 1:n
                    g(i,j)=1/(1+exp(-z(i,j)));
                end
            end
        end
    end
end



% =============================================================

end
