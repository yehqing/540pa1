function best_lambda = select_lambda_crossval(X,y,lambda_low,lambda_high,lambda_step)
% Select the best lambda for training set (X,y) by sweeping a range of
% lambda values from lambda_low to lambda_high in steps of lambda_step
%
% For each lambda value, divide the data into 10 equal folds
% using crossvalind. 
% Then, repeat i = 1:10:
%  1. Retain fold i for testing, and train logistic model on the other 9 folds
%  with that lambda
%  2. Evaluate accuracy of model on left out fold i
% Accuracy associated with that lambda is the averaged accuracy over all 10
% folds.
% Do this for each lambda in the range and pick the best one.

folds = 10;
best_lambda = 0;

m = size(X,1);
d = size(X,2);
Jmin = bitmax;

lambda = lambda_low;
while lambda <= lambda_high

	Jsum = 0;
	
	indices = crossvalind('Kfold',m,folds);
	for i = 1:folds
		% devide data here
		test=(indices==i);
		train=~test;
		Xtrain=X(train,:);
		Xval=X(test,:);
		ytrain=y(train,:);
		yval=y(test,:);

		initial_theta = zeros(d, 1);
		options = optimset('GradObj', 'on', 'MaxIter', 400);
		% Train theta	
		[theta, cost] = fminunc(@(t)(costFunctionReg(t, Xtrain, ytrain, lambda)), initial_theta, options);	    
		% Calculate cost
		[J, grad] = costFunction(theta, Xval, yval);
		
		Jsum = Jsum + J;
	end

%	for i = 1 : folds
%		% devide data here
%		Xtrain = X( 1 : i * m/10 , : );

	Jsum = Jsum / folds;

	if(Jsum < Jmin)
		best_lambda = lambda;
		Jmin = Jsum;
	end

	lambda = lambda + lambda_step;
end
