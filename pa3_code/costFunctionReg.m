function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta

y1 = sigmoid(X*theta);

for i = 1:m
    J= J+ (-y(i)*log(y1(i)))-((1-y(i))*log(1-y1(i)));
end
J=J/m;
temp = 0;
d=size(theta,1);
for i = 2:d
    temp = temp + theta(i)^2;
end
temp=temp*lambda/(2*m);
J = J +temp;

%theta 0 
for i = 1:m
    grad(1)=grad(1)+(y1(i)-y(i))*X(i,1);
end
grad(1)=grad(1)/m;

for j = 2:d
    for i = 1:m
        grad(j)=grad(j)+(y1(i)-y(i))*X(i,j);
    end
    grad(j)=grad(j)/m+lambda*theta(j)/m;
end



% =============================================================

end
