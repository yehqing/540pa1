'''

@author: Bonnie
'''

#imports
from PIL import Image
from sklearn import linear_model
from string import *
from math import *
import numpy as np
import glob
import os
import sys
import csv

class galaxyLinearRegression:
    def __init__(self, c, alpha, r, s, m, g):
        self.col = c   #range from 1 to 37
        self.learning_rate = alpha #0.003
        self.reg = r #0.0001
        self.pixelSize = s #8
        self.crop_dimensions = (146, 140, 284, 284)
            
        self.m_total = m #28500    # use how many data in total (m limit)
        self.m_gap = g #28500       # increase by m_gap when a certain value of m is done
        self.m_size = self.m_total/self.m_gap

        self.test_start = 50000;
        
        self.times = 1   # average error of how many times
        
        # range from 0 to 1,  rate_0 and rate_1 will split train_x into  size(train_x0) : size(train_x1) : size(train_x2) = rate_0 : rate_1 : 1-(rate_0+rate_1)
        self.rate_0 = 0.6 
        self.rate_1 = 0.2
        
        self.Jerror0 = [] 
        self.Jerror1 = []
        self.Jerror2 = []
        
        self.train_files_path = "D:/AAAAAA/540Project/images_training_rev1/*.jpg"     
        self.test_files_path = "D:/AAAAAA/540Project/images_training_rev0/*.jpg"
        self.trim1 = len("D:/AAAAAA/540Project/images_test_rev1/")
        self.train_sol_path = 'D:/AAAAAA/540Project/training_solutions_rev1.csv'
        
        self.num_train_files = len(glob.glob(self.train_files_path))
        self.num_test_files = len(glob.glob(self.test_files_path))

        self.train_x = np.zeros((self.m_total,144/self.pixelSize*144/self.pixelSize))
        self.train_y = np.zeros(self.m_total)
        
        self.test_x = np.zeros((self.num_test_files,144/self.pixelSize*144/self.pixelSize))
        self.test_y = np.zeros((self.num_test_files))

        self.prec = np.zeros((self.num_test_files))

    def ReadTrainData(self):
        
        print "reading training data..."

        # Read train_y
        with open(self.train_sol_path,'rU') as f:
            reader = csv.reader(f)
            i = 0
            classification = [[]]
            for row in reader:
                if i == 0:
                    i=i+1
                    continue
                classification.append(row)
                try:
                    self.train_y[i-1] = float(classification[i][self.col])
                    i=i+1
                except ValueError, e:
                    i=i+1

                if i > self.m_total:
                    break
                   
        # Read train_x
        for enum, infile in enumerate(glob.glob(self.train_files_path)):
            image=Image.open(infile)
            filen, ext = os.path.splitext(infile)
            file_id = filen[-6:]
            image = image.crop(self.crop_dimensions)
            image = image.convert('L')
            image = image.resize((image.size[0]/self.pixelSize,image.size[1]/self.pixelSize),Image.NEAREST)
            #print image.size[0]
            #image.show()
            pixel = image.load()

            if enum == self.m_total:               
                print enum, "images done..."
                break
            
            #read pixel information into matrix train_x
            for i in xrange(0,image.size[0]):
                for j in xrange(0,image.size[1]):
                    self.train_x[enum,i*image.size[0]+j]=round(float(pixel[i,j])/float(255),5)

            if enum%100 == 0:
                print enum, "images done..."
                    
        print "reading training data process done"


    def train_learning_curve(self):
        print "start training model"
        #np.random.seed(0)
        for i in xrange(0,self.m_size):
            m = (i+1)*self.m_gap
            
            J0 = 0
            J1 = 0
            J2 = 0
            
            size0 = int(m * self.rate_0)
            size1 = int(m * self.rate_1)
            size2 = m - size0 - size1

            train_x0 = np.zeros(( size0 ,144/self.pixelSize*144/self.pixelSize ))
            train_x1 = np.zeros(( size1 ,144/self.pixelSize*144/self.pixelSize ))
            train_x2 = np.zeros(( size2 ,144/self.pixelSize*144/self.pixelSize ))
            
            train_y0 = np.zeros( size0 )
            train_y1 = np.zeros( size1 )
            train_y2 = np.zeros( size2 )

            s = 0
            for t in xrange(size0+size1,m):  
                train_x2[s,:] = self.train_x[t,:]
                train_y2[s]=(self.train_y[t])
                s = s + 1
            
            for t in xrange(0, self.times):         
                #split train x into train_x0 and train_x1, using self.rate_0  
                index = np.arange(self.m_total)
                np.random.shuffle(index)

                k = 0
                for j in xrange(0,size0+size1):  
                    if j < size0 :
                        train_x0[j,:] = self.train_x[index[j],:]
                        train_y0[j]=(self.train_y[index[j]])
                    else :
                        train_x1[k,:] = self.train_x[index[j],:]
                        train_y1[k]=(self.train_y[index[j]])
                        k = k+1
                
                clf = linear_model.SGDRegressor(alpha=self.reg , learning_rate='constant' , eta0=self.learning_rate, warm_start=False)
                clf.fit(train_x0, train_y0)

                J0 += sqrt(np.power((clf.predict(train_x0)-train_y0),2).sum()/size0)
                J1 += sqrt(np.power((clf.predict(train_x1)-train_y1),2).sum()/size1)
                J2 += sqrt(np.power((clf.predict(train_x2)-train_y2),2).sum()/size2)

            J0 = J0/self.times
            J1 = J1/self.times
            J2 = J2/self.times
            
            self.Jerror0.append(J0)
            self.Jerror1.append(J1)
            self.Jerror2.append(J2)
            


    def generate_learning_curve_csv(self):
        f = open('learning_curve.csv', 'wb')
        try:
            writer = csv.writer(f)
            for i in xrange(0,self.m_size):
                Py = []
                Py.append((i+1)*self.m_gap)
                Py.append(self.Jerror0[i])
                Py.append(self.Jerror1[i])
                Py.append(self.Jerror1[i]-self.Jerror0[i])
                Py.append(self.Jerror2[i])
                writer.writerow(Py)
            Py = []
            Py.append(self.col)
            Py.append(self.pixelSize)
            Py.append(self.reg)
            writer.writerow(Py)
        finally:
                    f.close()
        print "learning_curve.csv done"

#======================================================================================

    def ReadTestData(self):
        print "reading test data..."

        # Read test_y
        with open(self.train_sol_path,'rU') as f:
            reader = csv.reader(f)
            i = -1
            classification = []
            for row in reader:
                if i < self.test_start:
                    i=i+1
                else:
                    classification.append(row)
                    try:
                        self.test_y[i-self.test_start] = float(classification[i-self.test_start][self.col])
                        i=i+1
                    except ValueError, e:
                        i=i+1


        # Read test_x
        x = np.zeros((self.num_test_files,144/self.pixelSize*144/self.pixelSize))
        for enum, infile in enumerate(glob.glob(self.test_files_path)):
            image=Image.open(infile)
            filen, ext = os.path.splitext(infile)
            file_id = filen[-6:]
            image = image.crop(self.crop_dimensions)
            image = image.convert('L')
            image = image.resize((image.size[0]/self.pixelSize,image.size[1]/self.pixelSize),Image.NEAREST)
            #print image.size[0]
            #image.show()
            pixel = image.load()
            
            #read pixel information into matrix x
            for i in xrange(0,image.size[0]):
                    for j in xrange(0,image.size[1]):
                        self.test_x[enum,i*image.size[0]+j]=round(float(pixel[i,j])/float(255),5)
            if enum%100 == 0:
                    print enum, "images done..."
        print "reading test data process done"        

    def train_and_predict(self):
        
        print "start training... "
        #np.random.seed(0)
        for i in xrange(0,self.m_size):
            m = (i+1)*self.m_gap
            
            size0 = int(m * self.rate_0)

            train_x0 = np.zeros(( size0 ,144/self.pixelSize*144/self.pixelSize ))          
            train_y0 = np.zeros( size0 )
            
            for t in xrange(0, self.times):         
                #split train x into train_x0 and train_x1, using self.rate_0  
                index = np.arange(self.m_total)
                np.random.shuffle(index)

                for j in xrange(0,size0):  
                    train_x0[j,:] = self.train_x[index[j],:]
                    train_y0[j]=(self.train_y[index[j]])
                
                clf = linear_model.SGDRegressor(alpha=self.reg , learning_rate='constant' , eta0=self.learning_rate, warm_start=False)
                clf.fit(train_x0, train_y0)
                self.prec = clf.predict(self.test_x)

    def ReadNormalizedTrainData(self):
        
        print "reading training data..."

        # Read train_y
        with open(self.train_sol_path,'rU') as f:
            reader = csv.reader(f)
            i = 0
            classification = [[]]
            for row in reader:
                if i == 0 :
                    i=i+1
                    continue

                if i >= self.m_total:
                    break
                
                classification.append(row)
                try:
                    if float(classification[i][self.col])+float(classification[i][self.col+1]) == 0:
                        self.train_y[i-1] = float(0)
                    else :
                        self.train_y[i-1] = float(classification[i][self.col])/(float(classification[i][self.col])+float(classification[i][self.col+1]))
                    i=i+1
                except ValueError, e:
                    i=i+1

                
                   
        # Read train_x
        for enum, infile in enumerate(glob.glob(self.train_files_path)):
            image=Image.open(infile)
            filen, ext = os.path.splitext(infile)
            file_id = filen[-6:]
            image = image.crop(self.crop_dimensions)
            image = image.convert('L')
            image = image.resize((image.size[0]/self.pixelSize,image.size[1]/self.pixelSize),Image.NEAREST)
            #print image.size[0]
            #image.show()
            pixel = image.load()

            if enum == self.m_total:               
                print enum, "images done..."
                break
            
            #read pixel information into matrix train_x
            for i in xrange(0,image.size[0]):
                for j in xrange(0,image.size[1]):
                    self.train_x[enum,i*image.size[0]+j]=round(float(pixel[i,j])/float(255),5)

            if enum%100 == 0:
                print enum, "images done..."
                    
        print "reading training data process done"


    def post_train_learning_curve(self):
        print "start training model"
        #np.random.seed(0)
        for i in xrange(0,self.m_size):
            m = (i+1)*self.m_gap
            
            J0 = 0
            J1 = 0
            J2 = 0
            
            size0 = int(m * self.rate_0)
            size1 = int(m * self.rate_1)
            size2 = m - size0 - size1

            train_x0 = np.zeros(( size0 ,144/self.pixelSize*144/self.pixelSize ))
            train_x1 = np.zeros(( size1 ,144/self.pixelSize*144/self.pixelSize ))
            train_x2 = np.zeros(( size2 ,144/self.pixelSize*144/self.pixelSize ))
            
            train_y0 = np.zeros( size0 )
            train_y1 = np.zeros( size1 )
            train_y2 = np.zeros( size2 )

            s = 0
            for t in xrange(size0+size1,m):  
                train_x2[s,:] = self.train_x[t,:]
                train_y2[s]=(self.train_y[t])
                s = s + 1
            
            for t in xrange(0, self.times):         
                #split train x into train_x0 and train_x1, using self.rate_0  
                index = np.arange(self.m_total)
                np.random.shuffle(index)

                k = 0
                for j in xrange(0,size0+size1):  
                    if j < size0 :
                        train_x0[j,:] = self.train_x[index[j],:]
                        train_y0[j]=(self.train_y[index[j]])
                    else :
                        train_x1[k,:] = self.train_x[index[j],:]
                        train_y1[k]=(self.train_y[index[j]])
                        k = k+1
                
                clf = linear_model.SGDRegressor(alpha=self.reg , learning_rate='constant' , eta0=self.learning_rate, warm_start=False)
                clf.fit(train_x0, train_y0)


                J0 += sqrt(np.power((np.multiply(clf.predict(self.test_x),self.prec)-self.test_y),2).sum()/self.num_test_files)
                #J1 += sqrt(np.power((clf.predict(train_x1)-train_y1),2).sum()/size1)
                #J2 += sqrt(np.power((clf.predict(train_x2)-train_y2),2).sum()/size2)

            J0 = J0/self.times
            J1 = J1/self.times
            J2 = J2/self.times
            
            self.Jerror0.append(J0)
            self.Jerror1.append(J1)
            self.Jerror2.append(J2)



if __name__ == '__main__':
    glr = galaxyLinearRegression(2 , 0.003 , 0.0001 , 8, 28500, 28500)
    glr.ReadTrainData()
    glr.ReadTestData()
    glr.train_and_predict()

    glr2 = galaxyLinearRegression(4 , 0.003 , 0.0001 , 8, 50000 ,500 )
    glr2.times = 10
    glr2.prec = glr.prec
    glr2.ReadNormalizedTrainData()
    glr2.ReadTestData()
    glr2.post_train_learning_curve()
    glr2.generate_learning_curve_csv()
