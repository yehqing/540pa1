function [theta, J_history] = gradientDescentMulti(X, y, theta, alpha, num_iters)
%GRADIENTDESCENTMULTI Performs gradient descent to learn theta
%   theta = GRADIENTDESCENTMULTI(x, y, theta, alpha, num_iters) updates theta by
%   taking num_iters gradient steps with learning rate alpha

% Initialize some useful values
m = length(y); % number of training examples
J_history = zeros(num_iters, 1);
theta_size = length(theta);
temp = zeros(1,length(theta));

for iter = 1:num_iters

    % ====================== YOUR CODE HERE ======================
    % Instructions: Perform a single gradient step on the parameter vector
    %               theta. 
    %
    % Hint: While debugging, it can be useful to print out the values
    %       of the cost function (computeCostMulti) and gradient here.
    %

    
    for i = 1:m
        count = X(i,:)*theta;
        for j = 1:theta_size
            
            temp(j) = temp(j) + (count - y(i))*X(i,j);
        end
            
    end
    temp = temp/ m;
    for i = 1:theta_size
        theta(i) = theta(i) -  alpha * temp(i);
    end




    % ============================================================

    % Save the cost J in every iteration    
    J_history(iter) = computeCostMulti(X, y, theta);
    % print the cost J at every iteration
end

end
